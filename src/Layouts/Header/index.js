import React from "react";
import { StarOutlined } from "@ant-design/icons";

import { StyleWrapper } from "./header.style.js";

const Header = ({ handleDarkMode, isDark, theme }) => {
  return (
    <StyleWrapper theme={theme}>
      <header>
        <span className="title">Where in the world?</span>
        <button onClick={handleDarkMode}>
          <StarOutlined />
          <span>{isDark ? "Dark" : "Light"} Mode</span>
        </button>
      </header>
    </StyleWrapper>
  );
};

export default Header;
