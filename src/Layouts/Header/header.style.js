import styled from "styled-components";

export const StyleWrapper = styled.div`
  header {
    padding: 20px 40px;
    display: flex;
    justify-content: space-between;
    margin: 0 auto;
    box-shadow: 1px 1px 10px ${(props) => props.theme.borderColor};

    .title {
      font-weight: 700;
      font-size: 22px;
    }

    button {
      border: none;
      outline: none;
      background-color: transparent;
      cursor: pointer;
      span {
        color: ${(props) => props.theme.color};
        padding-left: 5px;
      }
      svg {
        color: ${(props) => props.theme.color};
      }
    }
  }
`;
