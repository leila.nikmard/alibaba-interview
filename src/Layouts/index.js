import React, { Component } from "react";
import Header from "./Header";

class DefaultLayout extends Component {
  render() {
    return <Header />;
  }
}

export default DefaultLayout;
