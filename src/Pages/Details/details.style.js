import styled from "styled-components";

export const StyleWrapper = styled.div`
  .details-container {
    width: 80%;
    margin: 30px auto;
    padding: 35px 0;
    min-height: 100vh;

    .back-home {
      text-decoration-line: none;
      padding: 5px 15px;
      border-radius: 5px;
      box-shadow: 1px 1px 10px ${(props) => props.theme.borderColor};
      background-color: ${(props) => props.theme.backgroundColor};
      color: ${(props) => props.theme.color};
      border: 1px solid ${(props) => props.theme.borderColor};
    }
  }
`;
