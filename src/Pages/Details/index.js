import React, { useState, useEffect, useContext } from "react";
import FlagDetail from "../../components/Flag/FlagDetail";
import { getDetailsCountry } from "../../utils/services";
import { RollbackOutlined } from "@ant-design/icons";
import { ThemeContext } from "../../Context/theme";
import Loading from "../../components/Loading";
import { Link } from "react-router-dom";

import { StyleWrapper } from "./details.style.js";

const Details = ({ match }) => {
  const [detailsCountry, setDetailsCountry] = useState(null);
  const [loading, setLoading] = useState(false);
  const params = match?.params?.name;
  const [{ theme }] = useContext(ThemeContext);

  useEffect(() => {
    handleGetDetailsCountry();
  }, [params]);

  const handleGetDetailsCountry = () => {
    setLoading(true);
    getDetailsCountry(params)
      .then((response) => response.json())
      .then((res) => {
        setDetailsCountry(res);
        setLoading(false);
      });
  };

  return (
    <StyleWrapper theme={theme}>
      <div className="details-container">
        <Link to="/" className="back-home">
          <RollbackOutlined />
          <span>Back</span>
        </Link>
        {loading ? (
          <Loading />
        ) : (
          <FlagDetail data={detailsCountry} theme={theme} />
        )}
      </div>
    </StyleWrapper>
  );
};

export default Details;
