import styled from "styled-components";

export const StyleWrapper = styled.div`
  .form-container {
    display: flex;
    justify-content: space-between;
    padding: 40px;
    width: 70%;
    margin: 0 auto;
    @media (max-width: 768px) {
      flex-direction: column;
      justify-content: center;
    }
  }

  .flag-boxs-container {
    display: flex;
    flex-wrap: wrap;
    padding: 20px 40px;
    justify-content: space-between;
    width: 70%;
    margin: 0 auto;
    min-height: 90vh;
    a {
      background-color: ${(props) => props.theme.backgroundColor};
      color: ${(props) => props.theme.color};
      box-shadow: 1px 1px 10px ${(props) => props.theme.borderColor};
      text-decoration: none;
      margin-bottom: 50px;
    }

    @media (max-width: 768px) {
      justify-content: center;
    }
  }

  .input-search {
    width: 300px;
    border: none;
    outline: none;
    padding: 15px 5px;
    border-radius: 5px;
    box-shadow: 1px 1px 10px ${(props) => props.theme.borderColor};
    background-color: ${(props) => props.theme.backgroundColor};
    color: ${(props) => props.theme.color};
    @media (max-width: 768px) {
      margin-bottom: 20px;
      width: auto;
    }
  }

  select {
    border: none;
    outline: none;
    width: 150px;
    border-radius: 5px;
    padding: 15px 0;
    box-shadow: 1px 1px 10px ${(props) => props.theme.borderColor};
    color: ${(props) => props.theme.color};
    background-color: ${(props) => props.theme.backgroundColor};
  }
`;
