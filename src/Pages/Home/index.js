import React, { useState, useEffect, useContext } from "react";
import { Link } from "react-router-dom";
import { ThemeContext } from "../../Context/theme";
import Loading from "../../components/Loading";
import {
  loadAllCountries,
  getContinent,
  getDetailsCountry,
} from "../../utils/services";
import Flag from "../../components/Flag/Flag";
import SearchForm from "../../components/Search/Search";
import RegionSelector from "../../components/Select/RegionSelector";

import { StyleWrapper } from "./home.style.js";

const Home = () => {
  const [searchValue, setSearchValue] = useState("");
  const [countriesData, setCountriesData] = useState([]);
  const [loading, setLoading] = useState(false);
  const [{ theme }] = useContext(ThemeContext);

  const handleSearch = (e) => {
    const value = e.target.value;
    setSearchValue(value);
    setLoading(true);
    value
      ? getDetailsCountry(value)
          .then((response) => response.json())
          .then((res) => {
            setCountriesData(res);
            setLoading(false);
          })
      : handleLoadAllCountries();
  };

  const handleSelect = (e) => {
    const value = e.target.value;
    getContinent(value)
      .then((response) => response.json())
      .then((res) => {
        console.log("res", res);
      });
  };

  const handleLoadAllCountries = () => {
    setLoading(true);
    loadAllCountries()
      .then((response) => response.json())
      .then((res) => {
        setCountriesData(res);
        setLoading(false);
      });
  };

  useEffect(() => {
    handleLoadAllCountries();
  }, []);

  return (
    <StyleWrapper theme={theme}>
      <div className="form-container">
        <SearchForm onSearch={handleSearch} value={searchValue} />
        <RegionSelector onSelect={handleSelect} theme={theme} />
      </div>
      <div className="flag-boxs-container">
        {loading ? (
          <Loading />
        ) : countriesData.length > 0 ? (
          countriesData.map((item, index) => (
            <Link to={`/details/${item?.name}`} key={index}>
              <Flag data={item} />
            </Link>
          ))
        ) : (
          "Not Data"
        )}
      </div>
    </StyleWrapper>
  );
};

export default Home;
