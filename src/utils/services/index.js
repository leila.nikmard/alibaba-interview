import * as config from "../config";

export const loadAllCountries = () => fetch(`${config.API_BASE_URL}/all`);

export const getDetailsCountry = (name) =>
  fetch(`${config.API_BASE_URL}/name/${name}`);

export const getContinent = (region) =>
  fetch(`${config.API_BASE_URL}/continent/${region}`);
