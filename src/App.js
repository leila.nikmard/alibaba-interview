import React, { useContext } from "react";
import { ThemeContext } from "./Context/theme";
import Header from "./Layouts/Header";
import RouterComponent from "./routes";
import { StyleWrapper } from "./App.style";

function App() {
  const [{ theme, isDark }, toggleTheme] = useContext(ThemeContext);
  return (
    <StyleWrapper theme={theme}>
      <Header handleDarkMode={toggleTheme} isDark={isDark} theme={theme} />
      <RouterComponent />
    </StyleWrapper>
  );
}

export default App;
