import React from "react";

const SearchForm = ({ onSearch, searchValue }) => {
  return (
    <input
      className="input-search"
      placeholder="Search for a country..."
      type="text"
      value={searchValue}
      onChange={onSearch}
    />
  );
};

export default SearchForm;
