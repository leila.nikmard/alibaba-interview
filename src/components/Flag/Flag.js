import React from "react";

import { StyleWrapper } from "./flag.style";

const Flag = ({ data }) => {
  return (
    <StyleWrapper>
      <div className="flag-box">
        <img src={data.flag} width="100%" height={100} />
        <div className="data-country">
          <p className="country-name">{data.name}</p>
          <span>
            <span className="item-data-country">Pupolation: </span>
            <span className="value-data-country">{data.population}</span>
          </span>
          <span>
            <span className="item-data-country">Region: </span>
            <span className="value-data-country">{data.region}</span>
          </span>
          <span>
            <span className="item-data-country">Capital: </span>
            <span className="value-data-country">{data.capital}</span>
          </span>
        </div>
      </div>
    </StyleWrapper>
  );
};

export default Flag;
