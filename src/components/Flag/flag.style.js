import styled from "styled-components";

export const StyleWrapper = styled.div`
  .flag-box {
    width: 200px;
    border: 1px solid transparent;
    display: flex;
    flex-direction: column;
    margin-bottom: 20px;
    border-radius: 5px;
    overflow: hidden;

    @media (max-width: 768px) {
      width: 250px;
    }

    .data-country {
      display: flex;
      flex-direction: column;
      padding: 10px 12px 0px 12px;
      .country-name {
        font-weight: 800;
        font-size: 16px;
      }

      span {
        font-size: 14px;
        padding-bottom: 6px;
      }
      .value-data-country {
        color: hsl(0, 0%, 52%);
      }
      .item-data-country {
        font-weight: 500;
      }
    }
  }
`;

export const DetailsWrapper = styled.div`
  .flag-details-container {
    width: 100%;
    display: flex;
    padding: 60px 0;
    font-size: 16px;

    @media (max-width: 768px) {
      flex-direction: column;
    }

    .flag-image {
      width: 40%;
      display: flex;
      justify-content: start;
      align-items: center;
      @media (max-width: 768px) {
        width: 100%;
        img {
          width: 100%;
        }
      }
    }

    .flag-details {
      width: 60%;
      display: flex;
      flex-direction: column;
      justify-content: center;
      @media (max-width: 768px) {
        width: 100%;
      }
      .country-name {
        font-weight: 700;
        font-size: 20px;
      }
      .details {
        display: flex;
        flex-direction: column;
        flex-wrap: wrap;
        height: 150px;
        @media (max-width: 768px) {
          height: auto;
          flex-wrap: nowrap;
          margin-bottom: 20px;
        }
        span {
          padding-bottom: 8px;
          font-size: 16px;
          @media (max-width: 768px) {
            padding-bottom: 15px;
          }
        }

        .name {
          font-weight: 500;
        }

        .value {
          color: hsl(0, 0%, 52%);
          padding-left: 5px;
        }
      }
      button {
        margin-right: 8px;
        outline: none;
        padding: 4px 12px;
        border-radius: 5px;
        border: 1px solid ${(props) => props.theme.borderColor};
        background-color: ${(props) => props.theme.backgroundColor};
        color: ${(props) => props.theme.color};
      }

      .countries-btn {
        display: inline-block;
        @media (max-width: 768px) {
          padding: 15px 0;
          display: flex;
          justify-content: space-between;
          width: 100%;
        }
      }
    }
  }
`;
