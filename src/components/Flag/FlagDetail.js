import React from "react";
import { Link } from "react-router-dom";

import { DetailsWrapper } from "./flag.style";

const FlagDetail = ({ data, theme }) => {
  const details = data && data.find((it) => it);
  return (
    <DetailsWrapper theme={theme}>
      <div className="flag-details-container">
        <div className="flag-image">
          <img src={details?.flag} width="85%" />
        </div>
        <div className="flag-details">
          <p className="country-name">Germany</p>
          <div className="details">
            <span>
              <span className="name">Native Name:</span>
              <span className="value">{details?.nativeName}</span>
            </span>
            <span>
              <span className="name">Population:</span>
              <span className="value">{details?.population}</span>
            </span>
            <span>
              <span className="name">Region:</span>
              <span className="value">{details?.region}</span>
            </span>
            <span>
              <span className="name">Sub Region:</span>
              <span className="value">{details?.subregion}</span>
            </span>
            <span>
              <span className="name">Capital:</span>
              <span className="value">{details?.capital}</span>
            </span>
            <span>
              <span className="name">Top Level Domain:</span>
              <span className="value">{details?.topLevelDomain[0]}</span>
            </span>
            <span>
              <span className="name">Currencies:</span>
              <span className="value">{details?.currencies[0].code}</span>
            </span>
            <span>
              <span className="name">Langouages:</span>
              <span className="value">{details?.languages[0].name}</span>
            </span>
          </div>
          <div>
            <span>Border Countries: </span>
            <div className="countries-btn">
              {" "}
              <Link to="/details/france">
                <button>France</button>
              </Link>
              <Link to="/details/germany">
                <button>Germany</button>
              </Link>
              <Link to="/details/netherlands">
                <button>Netherlands</button>
              </Link>
            </div>
          </div>
        </div>
      </div>
    </DetailsWrapper>
  );
};

export default FlagDetail;
