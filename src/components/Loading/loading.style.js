import styled from "styled-components";

export const StyleWrapper = styled.div`
  width: 100%;
  .loading {
    font-size: 25px;
    display: flex;
    justify-content: center;
  }
`;
