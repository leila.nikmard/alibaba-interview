import React from "react";
import { StyleWrapper } from "./loading.style";

const Loading = () => {
  return (
    <StyleWrapper>
      <div className="loading">
        <p>Loading...</p>
      </div>
    </StyleWrapper>
  );
};

export default Loading;
