import React from "react";
import history from "./utils/helpers/history";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import Details from "./Pages/Details";
import Home from "./Pages/Home";

const RouterComponent = () => {
  return (
    <Router history={history}>
      <Switch>
        <Route path="/" exact component={Home}></Route>
        <Route path="/details/:name" component={Details}></Route>
      </Switch>
    </Router>
  );
};

export default RouterComponent;
