import { createContext, useState } from "react";

const themes = {
  dark: {
    backgroundColor: "hsl(207, 26%, 17%)",
    color: "hsl(0, 0%, 100%)",
    borderColor: "hsl(209, 23%, 22%)",
  },
  light: {
    backgroundColor: "hsl(0, 0%, 98%)",
    color: "hsl(200, 15%, 8%)",
    borderColor: "hsl(0, 0%, 90%)",
  },
};

export const ThemeContext = createContext();
export const ThemeProvider = ({ children }) => {
  const [isDark, setIsDark] = useState(false);
  const theme = isDark ? themes?.dark : themes?.light;
  const toggleTheme = () => {
    setIsDark(!isDark);
  };
  return (
    <ThemeContext.Provider value={[{ theme, isDark }, toggleTheme]}>
      {children}
    </ThemeContext.Provider>
  );
};
